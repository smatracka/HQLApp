<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body bgcolor="#80D0E0">
<div align="center">

<h1>Znajdź studenta - wynik</h1>

<table>
    <tr>
        <th>Nr indeksu</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Data urodzenia</th>
        <th>Płeć</th>
    </tr>
    <tr>
        <td>${student.bookNumber}</td>
        <td>${student.firstName}</td>
        <td>${student.lastName}</td>
        <td>${student.birthDate}</td>
        <td>${student.sexIndicator}</td>
    </tr>
</table>

</div>
<%@ include file="go_to_home.jspf" %>
</body>
