package deanery.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Student {

	@Id
	@GeneratedValue
	private Integer id;
	private Integer bookNumber;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private Boolean man;
	
	public String getSexIndicator() {
		if (Boolean.TRUE.equals(getMan())) {
			return "M";
		} else {
			return "K";
		}
	}

	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getBookNumber() {
		return bookNumber;
	}
	public void setBookNumber(Integer bookNumber) {
		this.bookNumber = bookNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getMan() {
		return man;
	}
	public void setMan(Boolean man) {
		this.man = man;
	}

}
