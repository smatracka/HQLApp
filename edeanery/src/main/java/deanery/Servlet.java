package deanery;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import deanery.command.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


@SuppressWarnings("serial")
@WebServlet( name = "servlet", urlPatterns = {"/servlet"}, loadOnStartup = 1)
public class Servlet extends HttpServlet {

	// Fabryka sesji Hibernate.
	private SessionFactory sessionFactory;
	
	private void doPostOrGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		if (commandEquals(request, "NewStudent")) {
			try {
				beginTransaction();
				NewStudent cmd = new NewStudent();
				cmd.setBookNumber(getIntegerParam(request, "bookNumber"));
				cmd.setFirstName(request.getParameter("firstName"));
				cmd.setLastName(request.getParameter("lastName"));
				cmd.setBirthDate(getDateParam(request, "birthDate"));
				cmd.setMan(Boolean.valueOf(request.getParameter("man").equals("M")));
				cmd.setSession(getSession());
				cmd.execute();
				forward(request, response, "/");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}

		if (commandEquals(request, "NewTeacher")) {
			try {
				beginTransaction();
				NewTeacher cmd = new NewTeacher();
				cmd.setFirstName(request.getParameter("firstName"));
				cmd.setLastName(request.getParameter("lastName"));
				cmd.setBirthDate(getDateParam(request, "birthDate"));
				cmd.setMan(Boolean.valueOf(request.getParameter("man").equals("M")));
				cmd.setSession(getSession());
				cmd.execute();
				forward(request, response, "/");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}

		if (commandEquals(request, "FindStudent")) {
			try {
				beginTransaction();
				FindStudent cmd = new FindStudent();
				cmd.setSession(getSession());
				cmd.setId(getIntegerParam(request, "id"));
				cmd.execute();
				request.setAttribute("student", cmd.getStudent());
				forward(request, response, "/find_student_result.jsp");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}

		if (commandEquals(request, "UpdateStudent")) {
			try {
				beginTransaction();
				FindStudent	cmd = new FindStudent();
				cmd.setSession(getSession());
				cmd.setId(getIntegerParam(request, "id"));
				cmd.execute();
				request.setAttribute("student", cmd.getStudent());
				forward(request, response, "/update_student_2.jsp");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}

		if (commandEquals(request, "UpdateStudent2")) {
			try {
				beginTransaction();
				UpdateStudent cmd = new UpdateStudent();
				cmd.setSession(getSession());
				cmd.setId(getIntegerParam(request, "id"));
				cmd.setBookNumber(getIntegerParam(request, "bookNumber"));
				cmd.setFirstName(request.getParameter("firstName"));
				cmd.setLastName(request.getParameter("lastName"));
				cmd.setBirthDate(getDateParam(request, "birthDate"));
				cmd.setMan(Boolean.valueOf(request.getParameter("man").equals("M")));
				cmd.execute();
				forward(request, response, "/");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}

		if (commandEquals(request, "DeleteStudent")) {
			try {
				beginTransaction();
				DeleteStudent cmd = new DeleteStudent();
				cmd.setSession(getSession());
				cmd.setId(getIntegerParam(request, "id"));
				cmd.execute();
				forward(request, response, "/");
				commit();
			} catch (Exception e) {
				rollback();
				throw new ServletException("ROLLBACK", e);
			}
			return;
		}
	}

	private Transaction beginTransaction() {
		return sessionFactory.getCurrentSession().beginTransaction();
	}
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	private void commit() {
		sessionFactory.getCurrentSession().getTransaction().commit();
	}

	private void rollback() {
		sessionFactory.getCurrentSession().getTransaction().rollback();
	}

	private Integer getIntegerParam(HttpServletRequest request, String name) {
		return new Integer(request.getParameter(name));
	}

	private Date getDateParam(HttpServletRequest request, String name) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter(name));
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String page)
			throws ServletException, IOException {
		request.getRequestDispatcher(page).forward(request, response);
	}

	private boolean commandEquals(HttpServletRequest request, String name) {
		return name.equals(request.getParameter("command"));
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPostOrGet(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPostOrGet(request, response);
	}

	public void init() throws ServletException {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
		builder.applySettings(cfg.getProperties());
		SessionFactory sessionFactory = cfg.buildSessionFactory(builder.build());
		this.sessionFactory = sessionFactory;
	}

}
